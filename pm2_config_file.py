import json ,sys ,os ,socket,random ,yaml
from creation_parse import ParseCreationMethods
from pprint import pprint


class Pm2ConfigFile:

    config_data = {}
    pm2_config = {}

    def __init__(self , appName):
        self.pm2_config={}
        self.creation_inst = ParseCreationMethods("app_name")
        with open('config.json') as data_file:
            config_data = json.load(data_file)
        self.app_name=appName
        self.config_data=config_data
    #genrate open port for app
    def generate_port(self):
        try:
            self.env={}
            self.port=random.randint(1111,9999)
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = sock.connect_ex(('127.0.0.1',self.port))
            if result == 0:
                return("Port is open")
            else:
                # edited_path=self.config_data.get("server_url",[]).split("http:")
                serv_ip=self.config_data.get("server_url",[])
                serv_url=str(serv_ip + ":"+ str(self.port)+"/p/"+self.app_name)
                end_point= "/p/"+self.app_name
                self.env[0]={"PORT":self.port,"VERBOSE":1,"PARSE_SERVER_SERVER_URL":serv_url,"PARSE_SERVER_MOUNT_PATH":end_point}

                string_port=str(self.port)
                os.system('sudo ufw allow '+ string_port)
                # os.system('sudo iptables -A OUTPUT -p tcp --sport '+ string_port +' -m conntrack --ctstate ESTABLISHED -j ACCEPT')
                return True
        except Exception as err:
            raise err
    #collect all data to one object
    def add_attributes_to_file(self):
        try:
            self.parse_dir=self.which("parse-server")
            self.app_dir=self.config_data.get("parse_app_dir",[])+self.app_name
            self.generate_port()
            self.pm2_config["apps"]=[{"name":str(self.app_name),"script":self.parse_dir,"watch":False,"merge_logs":True,"cwd":str(self.app_dir),"args":str(self.app_dir+"/parse-config.json"),"env":self.env[0]}]
            return True
        except Exception as err:
            raise err

    #add object with full date to yaml file
    def create_pm2_file(self):
        try:
            parse_app = self.config_data.get("parse_app_dir",[]) + self.app_name
            with open(parse_app + '/pm2.yaml', 'w') as outfile:
                yaml.dump(self.pm2_config, outfile)
            return True
        except Exception as err:
            raise err

    #to get installing directory of app
    def which(self,pgm):
        path=os.getenv('PATH')
        for p in path.split(os.path.pathsep):
            p=os.path.join(p,pgm)
            if os.path.exists(p) and os.access(p,os.X_OK):
                return p
