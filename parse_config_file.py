import json , sys ,os , uuid ,hashlib,time ,yaml
from creation_parse import ParseCreationMethods
from pprint import pprint


class ParseConfigFile:

    config_data = {}
    parse_config = {}
    hash_digest = []


    def __init__(self , appName):
        self.parse_config={}
        self.hash_digest = []
        self.creation_inst = ParseCreationMethods("app_name")
        with open('config.json') as data_file:
            config_data = json.load(data_file)
        self.app_name=appName
        self.config_data=config_data
        self.jsonfile_ids=["appId","masterKey","fileKey","clientKey","restAPIKey","javascriptKey","dotNetKey"]
        self.generate_key()

    def add_db_url(self):
        try:
            self.parse_config["databaseURI"]=self.config_data.get("db_url",[])
            return True
        except Exception as err:
            raise err
    #genrate hash random keys for app prefix on json configration file
    def generate_key(self):
        try:
            keys_prefixes = self.config_data.get("keys_prefixes",[])
            for i in keys_prefixes:
                hex_uuid=uuid.uuid4().hex
                sha256 = hashlib.sha256(hex_uuid.encode())
                coded_key = i + "-" + sha256.hexdigest().upper()
                self.hash_digest.append(coded_key)
            if (self.hash_digest.count == len(keys_prefixes)):
                return True
            else:
                return ("No Keys Genrated")
        except Exception as err:
            raise err

    def add_keys(self):
        try:
            for i,y in zip(self.hash_digest ,self.jsonfile_ids) :
                self.parse_config[y]=i
            return True
        except Exception as err:
            raise err


    def add_android_cert(self, senderId ,apiKey):
        try:
            if 'push' in self.parse_config:
                if 'android' in self.parse_config['push']:
                    self.parse_config['push']['android'] = {"senderId":senderId,"apiKey":apiKey}
                    return True
                else:
                    self.parse_config['push']['android'] = {"senderId":senderId,"apiKey":apiKey}
                    return True
            else:
                self.parse_config['push']={"android":{"senderId":senderId,"apiKey":apiKey}}
                return True
        except Exception as err:
            raise err

    def add_ios_cert(self,pfx , passphrase , bundleId, production):
        try:
            if 'push' in self.parse_config:
                if 'ios' in self.parse_config['push']:
                    self.parse_config['push']['ios'].append({"pfx":pfx,"passphrase":passphrase,"bundleId":bundleId,"production":production})
                    return True
                else:
                    self.parse_config['push']['ios'] = [{"pfx":pfx,"passphrase":passphrase,"bundleId":bundleId,"production":production}]
                    return True
            else:
                self.parse_config['push']={"ios":[{"pfx":pfx,"passphrase":passphrase,"bundleId":bundleId,"production":production}]}
                return True
        except Exception as err:
            raise err

    def add_cloud_code_path(self):
        self.parse_config["cloud"]=self.config_data["parse_app_dir"]+self.app_name+"/parse-app/cloud/main.js"
        return True
    #push object wich have all config data to json file
    def create_config_file(self):
        try:
            parse_app = self.config_data.get("parse_app_dir",[]) + self.app_name
            with open((parse_app + '/parse-config.json'), 'w') as outfile:
                json.dump(self.parse_config, outfile)
            return True
        except Exception as err:
            raise err

    def add_icon_path_to_server_config(self,path):
        self.icon_path=path

    #add app data to configration file of Parse dashboard 
    def add_app_to_parse_dashboard(self):
        with open(self.config_data.get("parse_app_dir",[])+self.app_name+'/pm2.yaml') as data_file:
            data = yaml.load(data_file)
        app_servver_url=str(self.config_data.get("kong_access_url",[])) + str(data["apps"][0]["env"]["PARSE_SERVER_MOUNT_PATH"])
        app_details={
                      "appName": self.app_name,
                      "icon": "self.icon_path",
                      "serverURL": app_servver_url,
                      "appId": self.hash_digest[0],
                      "masterKey": self.hash_digest[1]
                    }
        pas=uuid.uuid1().hex
        user_details={
            "user":self.app_name,
            "pass":pas,
            "apps":[
                {"appId":self.hash_digest[0]}
            ]
        }
        with open('/var/parse-apps/parse-dashboard/config.json') as data_file:
            server_config_data = json.load(data_file)
            server_config_data["apps"].append(app_details)
            server_config_data["users"].append(user_details)

        with open('/var/parse-apps/parse-dashboard/config.json', 'w') as outfile:
            json.dump(server_config_data, outfile)

        os.system('pm2 start /var/parse-apps/parse-dashboard/pm2.yaml')

        return True
