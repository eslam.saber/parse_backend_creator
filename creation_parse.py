import json , sys ,os,yaml ,time
from pprint import pprint
from pymongo import MongoClient
from git import Repo
import requests

class ParseCreationMethods:
    config_data = {}

    def __init__(self, appName):
        with open('config.json') as data_file:
            config_data = json.load(data_file)
        self.app_name = appName
        self.config_data = config_data

    # to clone app template from Git to Dir in Server
    def create_app_dir(self):
        parse_app = self.config_data.get("parse_app_dir",[]) + self.app_name
        try:
            # if not os.path.exists(parse_app):
            #     os.makedirs(parse_app, mode=0o777, exist_ok=False)
            #     os.makedirs(parse_app + "/cert", mode=0o777, exist_ok=False)
            #     os.makedirs(parse_app + "/cloud", mode=0o777, exist_ok=False)
            #     with open((parse_app + "/cloud/main.js"), 'w') as outfile:
            #         outfile.write("Parse.Cloud.define('hello', function(req, res) {res.success('"+self.app_name+"');});\n")
            if not os.path.exists(parse_app):
                os.makedirs(parse_app, mode=0o777)
                Repo.clone_from("https://gitlab.com/eslam.saber/parse-server-template.git", parse_app)
                return True
            else:
                return("App Is Exist")
        except Exception as err:
            raise err

    #to create New collection in MongoDb With App Name
    def create_db(self):
        try:
            client = MongoClient(self.config_data.get("db_url",[]))
            db_name = self.app_name
            db = client[self.app_name]
            collection = db[self.app_name]
            document = collection.insert_one({"AppName":self.app_name})
            return True
        except Exception as err:
            raise err
    '''
    Aftr Creating yaml and json config file it's to Run app by yaml and make
    Request for app url and make sure app running from response
    '''

    def run_app(self):
        try:
            yaml_dir = self.config_data["parse_app_dir"] + self.app_name + "/pm2.yaml"
            os.system('pm2 start ' + yaml_dir)

            with open(self.config_data.get("parse_app_dir", []) + self.app_name+'/pm2.yaml') as data_file:
                data = yaml.load(data_file)
            app_url = data["apps"][0]["env"]["PARSE_SERVER_SERVER_URL"] + "/functions/hello"

            with open(self.config_data.get("parse_app_dir", []) + self.app_name + '/parse-config.json') as config_file:
                keys_config = json.load(config_file)
            headers = {"X-Parse-Application-Id": keys_config['appId'], "X-Parse-REST-API-Key":keys_config["restAPIKey"],"Content-Type": "application/json"}
            time.sleep(10)# this made to make app delay till creating app on server to add it in dashboard

            req = requests.post(app_url, verify=False, headers=headers)
            result = json.loads(req.text)
            if result["result"] == self.app_name:
                return True
            else:
                return ("problem in publish app")
        except Exception as err:
            raise err

    '''
    it's configure kong web server to open app port on server and make request
    to make sure it's running
    '''
    def configure_kong(self):
        try:
            #Get App Data From Pm2 Yaml File
            with open(self.config_data.get("parse_app_dir", []) + self.app_name + '/pm2.yaml') as data_file:
                data = yaml.load(data_file)
            app_domain = data["apps"][0]["env"]["PARSE_SERVER_SERVER_URL"]
            app_end_point = data["apps"][0]["env"]["PARSE_SERVER_MOUNT_PATH"]
            app_port = data["apps"][0]["env"]["PORT"]
            #Open The API Ports in os Firewall
            string_port = str(app_port)
            os.system('sudo iptables -A INPUT -p tcp --dport ' + string_port + ' -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT')
            os.system('sudo iptables -A OUTPUT -p tcp --sport ' + string_port + ' -m conntrack --ctstate ESTABLISHED -j ACCEPT')

            headers = {"Content-Type": "application/x-www-form-urlencoded"}
            params = {"name": self.app_name,
                    "upstream_url": self.config_data.get("server_url", []) + ":" + string_port,
                    "request_path": app_end_point, }

            knog_server = self.config_data.get("kong_creation_url", [])
            # pprint(headers)
            # pprint(params)
            # pprint(knog_server+"/apis/")
            req = requests.post(knog_server + "/apis/", verify=False, params=params,headers=headers)
            pprint(req.status_code)
            if req.status_code == 200 or req.status_code == 201:
                return True
            else:
                return False
        except Exception as err:
            raise err
