from creation_parse import ParseCreationMethods
from parse_config_file import ParseConfigFile
from pm2_config_file import Pm2ConfigFile
from pprint import pprint
import sys

'''
this module to handle cases of creation or update prase apps
also if you will use click framework to call this modules from here
'''
def create_new_app(appName):
    try:

        #creation functions (database & app directory)
        creation_functions=ParseCreationMethods(appName)
        db_creation = creation_functions.create_db()
        dir_creation = creation_functions.create_app_dir()

        #create new config file for parse app (.json)
        parse_config_functions = ParseConfigFile(appName)
        db_url_adding = parse_config_functions.add_db_url()
        keys_adding = parse_config_functions.add_keys()
        #android_cert_adding = parse_config_functions.add_android_cert("senderId", "apiKey")
        #ios_cert_adding = parse_config_functions.add_ios_cert("pfx", "passphrase", "bundleId" , "production")
        cloud_path_adding =parse_config_functions.add_cloud_code_path()
        parse_creation_file= parse_config_functions.create_config_file()

        #create new config file for pm2 (.yaml)
        pm2_config_functions = Pm2ConfigFile(appName)
        port_generating = pm2_config_functions.generate_port()
        attributes_adding = pm2_config_functions.add_attributes_to_file()
        pm2_creation_file = pm2_config_functions.create_pm2_file()

        #run app after config creation
        creation_app = creation_functions.run_app()
        kong_config = creation_functions.configure_kong()

        #add app to parse dashboard
        if creation_app and kong_config is True:
            adding_to_dashboard = parse_config_functions.add_app_to_parse_dashboard()
            if adding_to_dashboard is True:
                return("App "+appName+" is created and published")
            else:
                return ('there is an error in publishing app on dashboard')
        else:
            return('there is an error in creating or configure app on kong')




    except Exception as err:
        raise err



if __name__ == '__main__':
    user_app_name = str(raw_input("add app name:"))
    result=create_new_app(user_app_name)
    pprint(result)
